<?php

use Task2\Controller;

require "./bootstrap.php";

try {

    $way = getParam('way');
    $collectionName = getParam('collectionName');
    $brands = getBrands($way, $collectionName);

    echo $twig->render('brands.html', ['brands' => print_r($brands, true)]);

} catch (\Exception $exception) {
    echo $twig->render('error.html', ['error' => $exception->getMessage()]);
}

function getParam($name, $default = null)
{
    return filter_var($_GET[$name] ?? ($default ?? ''), FILTER_SANITIZE_STRING);
}

function getBrands($way, $collectionName)
{
    $controller = new Controller();
    switch ($way) {
        case 'unordered':
            return $controller->getUnorderedBrandService()->getBrandsForCollection($collectionName);
            break;
        case 'itemsCountOrder':
            return $controller->getItemsCountOrderedBrandService()->getBrandsForCollection($collectionName);
            break;
        case 'itemsSortedByName':
            return $controller->getItemsSortByNameBrandService()->getBrandsForCollection($collectionName);
            break;
        default:
            throw new \Exception('Provided BrandService name [' . $way . '] is not supported.');
    }
}