<?php

namespace Task2\Entity;

use Task2\Validator\Validator;
use PhpJsonMarshaller\Annotations\MarshallProperty;

/**
 * Represents a single item from a search result.
 *
 */
class Item
{
    /**
     * @var Validator
     */
    private $validator;

    public function __construct()
    {
        $this->validator = new Validator();
    }

    /**
     * @MarshallProperty(name="name", type="string")
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @MarshallProperty(name="name", type="string")
     * @param string $name
     * @return Item
     */
    public function setName(string $name): Item
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @MarshallProperty(name="url", type="string")
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @MarshallProperty(name="url", type="string")
     * @param string $url
     * @return Item
     */
    public function setUrl(string $url): Item
    {
        $this->validator->validateUrl($url);
        $this->url = $url;
        return $this;
    }

    /**
     * @MarshallProperty(name="prices", type="\Task2\Entity\Price[]")
     * @return Price[]
     */
    public function getPrices(): array
    {
        return $this->prices;
    }

    /**
     * @MarshallProperty(name="prices", type="\Task2\Entity\Price[]")
     * @param Price[] $prices
     * @return Item
     */
    public function setPrices(array $prices): Item
    {
        $this->prices = $prices;
        return $this;
    }

    /**
     * Name of the item
     *
     * @var string
     */
    public $name;

    /**
     * Url of the item's page
     *
     * @var string
     */
    public $url;

    /**
     * Unsorted list of prices received from the
     * actual search query.
     *
     * @var Price[]
     */
    public $prices = [];
}
