<?php

namespace Task2\Entity;

use PhpJsonMarshaller\Annotations\MarshallProperty;

/**
 * Represents a single price from a search result
 * related to a single item.
 *
 */
class Price
{
    /**
     * Description text for the price
     *
     * @MarshallProperty(name="description", type="string")
     * @var string
     */
    public $description;

    /**
     * Price in euro
     *
     * @MarshallProperty(name="priceInEuro", type="int")
     * @var int
     */
    public $priceInEuro;

    /**
     * Warehouse's arrival date (to)
     *
     * @MarshallProperty(name="arrival", type="string")
     * @var \DateTime
     */
    public $arrivalDate;

    /**
     * Due to date,
     * defining how long will the item be available for sale (i.e. in a collection)
     *
     * @MarshallProperty(name="due", type="string")
     * @var \DateTime
     */
    public $dueDate;
}
