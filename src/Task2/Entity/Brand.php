<?php

namespace Task2\Entity;

use PhpJsonMarshaller\Annotations\MarshallProperty;

/**
 * Represents a single brand in the result.
 *
 */
class Brand
{
    /**
     * Name of the brand
     *
     * @MarshallProperty(name="name", type="string")
     * @var string
     */
    public $brand;

    /**
     * Brand's description
     *
     * @MarshallProperty(name="description", type="string")
     * @var string
     */
    public $description;

    /**
     * Unsorted list of items with their corresponding prices.
     *
     * @MarshallProperty(name="items", type="\Task2\Entity\Item[]")
     * @var Item[]
     */
    public $items = [];
}
