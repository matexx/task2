<?php

namespace Task2\Entity;

use PhpJsonMarshaller\Annotations\MarshallProperty;

class Collection
{

    /**
     * @MarshallProperty(name="collection", type="string")
     * @var string
     */
    public $collection;

    /**
     * @MarshallProperty(name="id", type="int")
     * @var integer
     */
    public $id;

    /**
     * @MarshallProperty(name="brands", type="\Task2\Entity\Brand[]")
     * @var Brand[]
     */
    public $brands;
}