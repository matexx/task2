<?php

namespace Task2;

use Task2\Datasource\CollectionDatasource;
use Task2\Service\Brand\BrandServiceProvider;
use Task2\Service\Collection\CollectionDataProvider;
use PhpJsonMarshaller\Decoder\ClassDecoder;
use PhpJsonMarshaller\Marshaller\JsonMarshaller;
use PhpJsonMarshaller\Reader\DoctrineAnnotationReader;

class Controller
{
    private $brandServiceProvider;

    /**
     * Controller constructor.
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function __construct()
    {
        $marshaller = new JsonMarshaller(new ClassDecoder(new DoctrineAnnotationReader()));

        $collectionDataProvider = new CollectionDataProvider(new CollectionDatasource(), $marshaller);

        $this->brandServiceProvider = new BrandServiceProvider($collectionDataProvider);
    }

    public function getUnorderedBrandService()
    {
        return $this->brandServiceProvider->getUnorderedBrandService();
    }

    public function getItemsCountOrderedBrandService()
    {
        return $this->brandServiceProvider->getItemsCountOrderedBrandService();
    }

    public function getItemsSortByNameBrandService()
    {
        return $this->brandServiceProvider->getItemNameOrderedBrandService();
    }

}