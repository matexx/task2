<?php

namespace Task2\Service\Collection;

use Task2\Datasource\CollectionDatasource;
use PhpJsonMarshaller\Exception\JsonDecodeException;
use PhpJsonMarshaller\Exception\UnknownPropertyException;

class CollectionDataProvider implements CollectionDataProviderInterface
{

    /**
     * @var CollectionDatasource
     */
    private $datasource;

    private $marshaller;

    public function __construct(CollectionDatasource $datasource, $marshaller)
    {
        $this->datasource = $datasource;
        $this->marshaller = $marshaller;
    }

    /**
     * This method should read from a datasource (JSON for case study)
     * and should return an unsorted list of brands found in the datasource.
     *
     * @param int $collectionId
     *
     * @return \Task2\Entity\Brand[]
     * @throws JsonDecodeException
     * @throws UnknownPropertyException
     */
    public function getResultForCollectionId(int $collectionId)
    {
        $jsonRawData = $this->datasource->getRaw($collectionId);
        $collection = $this->marshaller->unmarshall($jsonRawData, '\Task2\Entity\Collection');

        return $collection->brands;
    }
}