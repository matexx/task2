<?php

namespace Task2\Service\Collection;

/**
 * Represents the connection to a specific item store.
 * For the case study we will pretend we have only one item store to make things easier.
 */
interface CollectionDataProviderInterface
{
    /**
     * This method should read from a datasource (JSON for case study)
     * and should return an unsorted list of brands found in the datasource.
     *
     * @param int $collectionId
     *
     * @return \Task2\Entity\Brand[]
     */
    public function getResultForCollectionId(int $collectionId);
}
