<?php

namespace Task2\Service\Collection;

use UnexpectedValueException;

class CollectionNameResolver
{
    /**
     * Maps from collection name to the id for the item service.
     *
     * @var []
     */
    private $collectionNameToIdMapping = [
        "winter" => 1315475,
        "test" => 123456
    ];

    /**
     * @param string $collectionName
     * @return mixed
     * @throws UnexpectedValueException
     */
    public function resolve(string $collectionName)
    {
        if (empty($this->collectionNameToIdMapping[$collectionName])) {
            throw new UnexpectedValueException(sprintf('Provided collection name [%s] is not mapped.',
                $collectionName));
        }
        return $this->collectionNameToIdMapping[$collectionName];
    }
}