<?php

namespace Task2\Service\Brand;

use Task2\Service\Collection\CollectionNameResolver;
use Task2\Service\Collection\CollectionDataProviderInterface;
use Task2\Sorter\SorterInterface;

abstract class AbstractBrandService implements BrandServiceInterface
{
    /**
     * @var CollectionDataProviderInterface
     */
    protected $collectionDataProvider;

    /**
     * @var CollectionNameResolver
     */
    protected $nameResolver;

    /**
     * @var SorterInterface
     */
    protected $sorter = null;

    /**
     * @param CollectionDataProviderInterface $collectionDataProvider
     * @param CollectionNameResolver $nameResolver
     * @param SorterInterface|null $sorter
     */
    public function __construct(
        CollectionDataProviderInterface $collectionDataProvider,
        CollectionNameResolver $nameResolver,
        SorterInterface $sorter = null
    ) {
        $this->collectionDataProvider = $collectionDataProvider;
        $this->nameResolver = $nameResolver;
        $this->sorter = $sorter;
    }
}