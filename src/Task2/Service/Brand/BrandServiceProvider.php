<?php

namespace Task2\Service\Brand;

use Task2\Service\Collection\CollectionNameResolver;
use Task2\Sorter\ArraySorter;

class BrandServiceProvider
{
    private $collectionDataProvider;

    public function __construct($itemStoreService)
    {
        $this->collectionDataProvider = $itemStoreService;
    }

    public function getUnorderedBrandService()
    {
        return new UnorderedBrandService($this->collectionDataProvider, new CollectionNameResolver());
    }

    public function getItemsCountOrderedBrandService()
    {
        $sortBrandsByItemsCount = new ArraySorter(function ($brandX, $brandY) {
            return (count($brandY->items) <=> count($brandX->items));
        });
        return new ItemsCountOrderedBrandService($this->collectionDataProvider, new CollectionNameResolver(),
            $sortBrandsByItemsCount);
    }

    public function getItemNameOrderedBrandService()
    {
        $sortItemsByItemName = new ArraySorter(function ($itemX, $itemY) {
            return ($itemX->name <=> $itemY->name);
        });
        return new ItemNameOrderedBrandService($this->collectionDataProvider, new CollectionNameResolver(),
            $sortItemsByItemName);
    }
}