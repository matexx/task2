<?php

namespace Task2\Service\Brand;

use Task2\Service\Collection\CollectionDataProviderInterface;

class UnorderedBrandService extends AbstractBrandService implements BrandServiceInterface
{

    /**
     * @param string $collectionName Name of the collection to search for.
     *
     * @return \Task2\Entity\Brand[]
     */
    public function getBrandsForCollection(string $collectionName)
    {
        $collectionId = $this->nameResolver->resolve($collectionName);
        $result = $this->collectionDataProvider->getResultForCollectionId($collectionId);
        return $result;
    }

    /**
     * This is supposed to be used for testing purposes.
     * You should avoid replacing the item service at runtime.
     *
     * @param \Task2\Service\Collection\CollectionDataProviderInterface $collectionDataProvider
     *
     * @return void
     */
    public function setCollectionDataProvider(CollectionDataProviderInterface $collectionDataProvider)
    {
        $this->collectionDataProvider = $collectionDataProvider;
    }
}
