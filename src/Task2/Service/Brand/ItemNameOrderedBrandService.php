<?php

namespace Task2\Service\Brand;

use Task2\Service\Collection\CollectionDataProviderInterface;
use Task2\Sorter\SorterInterface;

class ItemNameOrderedBrandService extends AbstractBrandService implements BrandServiceInterface
{

    /**
     * @param string $collectionName Name of a collection to search for
     *
     * @return \Task2\Entity\Brand[]
     */
    public function getBrandsForCollection(string $collectionName)
    {
        $collectionId = $this->nameResolver->resolve($collectionName);
        $result = $this->collectionDataProvider->getResultForCollectionId($collectionId);
        if ($this->sorter instanceof SorterInterface) {
            foreach ($result as $brandKey => $brand) {
                $result[$brandKey]->items = $this->sorter->sort($brand->items);
            }
        }
        return $result;
    }

    /**
     * This is supposed to be used for testing purposes.
     * You should avoid replacing the item service at runtime.
     *
     * @param CollectionDataProviderInterface $collectionDataProvider
     * @return void
     */
    public function setCollectionDataProvider(CollectionDataProviderInterface $collectionDataProvider)
    {
        $this->collectionDataProvider = $collectionDataProvider;
    }
}