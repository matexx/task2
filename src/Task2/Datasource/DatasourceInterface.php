<?php

namespace Task2\Datasource;


interface DatasourceInterface
{

    /**
     * @param int $id
     * @return bool|string
     */
    public function getRaw(int $id);
}