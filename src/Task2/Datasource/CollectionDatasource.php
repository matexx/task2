<?php

namespace Task2\Datasource;


class CollectionDatasource implements DatasourceInterface
{
    /**
     * @param int $id
     * @return bool|string
     */
    public function getRaw(int $id){
        $filePath = __DIR__  . '/../../../data/'.$id.'.json';
        return file_get_contents($filePath);
    }
}