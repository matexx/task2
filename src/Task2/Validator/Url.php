<?php

namespace Task2\Validator;

use InvalidArgumentException;

Trait Url
{
    /**
     * @param $url string
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    public function validateUrl($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED)) {
            throw new InvalidArgumentException("Url: " . $url . " seems to be invalid!");
        }
        return true;
    }
}