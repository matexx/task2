<?php
/**
 * Created by PhpStorm.
 * User: Mateusz
 * Date: 30.03.2018
 * Time: 21:23
 */

namespace Task2\Sorter;

interface SorterInterface
{
    /**
     * @param array $dataArray
     * @return array
     */
    function sort($dataArray);
}