<?php

namespace Task2\Sorter;

use Task2\Entity\Brand;

class ArraySorter implements SorterInterface
{

    private $comparisonMethod;

    public function __construct(\Closure $comparisonMethod)
    {
        $this->comparisonMethod = $comparisonMethod;
    }

    /**
     * @param Brand[] $arrayToSort
     * @return array
     */
    public function sort($arrayToSort): array
    {
        usort($arrayToSort, $this->comparisonMethod);
        return $arrayToSort;
    }
}