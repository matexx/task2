<?php

use Task2\Service\Collection\CollectionNameResolver;

class CollectionNameResolverTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException \UnexpectedValueException
     */
    public function testResolvingUnmappedName()
    {
        $unmappedCollectionName = "unmappedCollectionName";
        $collectionNameResolver = new CollectionNameResolver();
        $collectionNameResolver->resolve($unmappedCollectionName);
    }

    public function testResolvingMappedName()
    {
        $mappedCollectionName = "winter";
        $mappedCollectionId = 1315475;

        $collectionNameResolver = new CollectionNameResolver();
        $result = $collectionNameResolver->resolve($mappedCollectionName);
        $this->assertEquals($mappedCollectionId, $result, "Should return proper id from map.");
    }
}