<?php

use Task2\Sorter\ArraySorter;

class ArraySorterTest extends \PHPUnit_Framework_TestCase
{
    private $isComparisonFunctionCalled = false;

    public function testCallComparisonFunction()
    {
        $this->isComparisonFunctionCalled = false;
        $dummyComparisonFunction = function () {
            $this->isComparisonFunctionCalled = true;
        };
        $dummyArrayToSort = [1,2,3];
        $arraySorter = new ArraySorter($dummyComparisonFunction);

        $arraySorter->sort($dummyArrayToSort);

        $this->assertTrue($this->isComparisonFunctionCalled, "Comparison function passed to constructor should be called.");
    }
}