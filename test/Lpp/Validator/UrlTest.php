<?php

use Task2\Validator\Url;

class UrlTest extends \PHPUnit_Framework_TestCase
{
    use Url;

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testValidationIncorrectUrl()
    {
        $someInvalidUrl = "someInvalidUrl";
        $this->validateUrl($someInvalidUrl);
    }

    public function testValidationCorrectUrl()
    {
        $validUrl = "http://valid.cp";
        $result = $this->validateUrl($validUrl);
        $this->assertTrue($result, "Should return true");
    }
}