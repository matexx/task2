# Case study

#### Setup project:

```
composer install
```

#### Project structure (simplified):
```
.case_study
├── data - <<dane json>>
├── _src
|   └── Task2
|       ├── Datasource - <<Class to provide raw data>>
|       ├── Entity - <<Data entities>>
|       ├── _Service
|           ├── Brand - <<Classes  related with brand data>>
|           └── Collection - <<Classes related with whole collection data
|       ├── Sorter - <Classes responsibile for sorting>>
|       ├── Validator - <<Validation stuff>>
|       └──Controller.php - <<Exposing data serving methods>>
├── templates - <<simple templates to present data>>
├── test - <<tests>>
├── vendor - <<External libraries>>
├── bootstrap.php - <<Autoloading and templates tool>>
├── brands.php - <<Presenting data>>
└── index.php - <<Entry point>>
```

#### Used external libraries 
- https://github.com/AnujRNair/php-json-marshaller
I've decided to use this library because i'ts very elastic - one annotation in every property or setter/getter in entity is enough, to map loaded json to class structure. 
Additional pros for use this library, is that was easy to apply validation of item url field in natural place for it - to entity setter. Also, adding another validation rules to any other fields is now pretty simple.
One cons of this library is changing keys in arrays - but of course, project architecture allows in easy way to change this library to another, if needed.
- https://github.com/twigphp/Twig
    Simple, well known template engine

## Subtasks
#### 1. Implement a basic ItemService
As I wrote above - I've used external library for map and transform JSON data to class structure.
I have created datasource (Task2\Datasource\DatasourceInterface) to provide access to json files.
I have also renamed ItemService to CollectionDataProvider - firstly - because it is more suitable name in my opinion. Second reason is that previous name was similar to Item Entity, and rename should prevent any occasion to make a mistake.

#### 2. Build a basic validator for the Item Entity
I've decided to create simple, reusable validation class based on traits. It's plugged into entity field setter to validate value on the converting json -> Objects stage. But of course it can be used in any other place of the system if needed.

#### 3. Implement a way to get different implementations of the BrandServiceInterface
I have added two additional implementations of BrandServiceInterface: 
- **Task2\Service\Brand\ItemNameOrderedBrandService** - result brands are in default order, but items in each brands as sorted by name. 
- **Task2\Service\Brand\ItemsCountOrderedBrandService** - result brands are sorted by number of items 

Reason of that is I want to show, how to use my simple sort mechanism, and how to add and apply new sorting rules.

## Additional things

There was question in comment in Task2\Service\Brand\BrandServiceInterface about thoughts on this interface multi-responsibilities approach.
In my opinion this is not so good way to plan architecture. This way breaks first SOLID rule - SRP. It's easier to maintain, test and modify a class, when we have only one reason to change this class.

## Author

Mateusz Orłowski
orlowski.mateusz@gmail.com

Feel free to ask anything.
