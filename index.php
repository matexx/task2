<?php

require "./bootstrap.php";

$parsedown = new Parsedown();
$readmeContent = file_get_contents('README.md');

$readme = $parsedown->text($readmeContent ?? '');

echo $twig->render('index.html', ['readme' => $readme]);